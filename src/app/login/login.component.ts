import { Component, OnInit } from '@angular/core';
import { slideToBottom, expand } from '../router.animations';
import { Router } from '@angular/router'; 

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [slideToBottom(), expand()]
})
export class LoginComponent implements OnInit {
  logoClass: string = '';
  showLogin: boolean = true;

  constructor(private _router: Router) { }

  ngOnInit() {
    this.logoClass = 'open';
  }

  login() {
    this.showLogin = false;
    let that = this;
    setTimeout(function () {
      that._router.navigate(['']);
    }, 300);
  }
}
