import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: 'naira'
})
export class NairaPipe implements PipeTransform {
    transform(value: number, args?: any): any {
        let reverse = value.toString().split('').reverse().join('');
        let returnval = '';
        for(let i = 0;i < reverse.length;i++){
            if(i > 0 && i%3 == 0){
                returnval+=',';
            }
            returnval+=reverse[i];
        }
        return returnval.toString().split('').reverse().join('');
    }
}