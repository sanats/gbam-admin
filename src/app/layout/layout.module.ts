import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { LayoutComponent } from './layout.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ProfileComponent } from './profile/profile.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LockscreenComponent } from './lockscreen/lockscreen.component';

import { SidebarModule } from 'primeng/sidebar';
import { BlockUIModule } from 'primeng/blockui';
import { TooltipModule } from 'primeng/tooltip';
import { NairaPipe } from '../utilties/app.pipes';

const layoutRoutes: Routes = [
  {
    path: '', component: LayoutComponent, children: [
      { path: '', component: DashboardComponent },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'profile', component: ProfileComponent }
    ]
  },

];

@NgModule({
  imports: [
    CommonModule,
    SidebarModule,
    BlockUIModule,
    TooltipModule,
    RouterModule.forChild(layoutRoutes)
  ],
  declarations: [LayoutComponent, DashboardComponent, NairaPipe, NavbarComponent, SidebarComponent, ProfileComponent, LockscreenComponent],
  exports: [RouterModule],
  providers: []
})
export class LayoutModule { }
