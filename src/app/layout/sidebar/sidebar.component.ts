import { Component, OnInit, Input } from '@angular/core';
import { MenuModule } from 'primeng/menu';
import { MenuItem } from 'primeng/api';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  displayFull: boolean = false;
  constructor() { }

  ngOnInit() {
  }

  toggleSidebar(){
    if(this.displayFull){
      this.closeNav();
    }else{
      this.openNav();
    }
  }

  openNav() {
    document.getElementById("sidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
    document.getElementById("navbar").style.marginLeft = "250px";
    this.displayFull = true;
  }

  closeNav() {
    document.getElementById("sidenav").style.width = "70px";
    document.getElementById("main").style.marginLeft = "70px";
    document.getElementById("navbar").style.marginLeft = "70px";
    this.displayFull = false;
  }

}
