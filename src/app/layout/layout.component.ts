import { Component, OnInit } from '@angular/core';
import { slideToRight } from './../router.animations';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
  animations: [slideToRight()]
})
export class LayoutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
