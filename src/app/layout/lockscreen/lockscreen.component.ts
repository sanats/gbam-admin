import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { slideToTop } from '../../router.animations';
import { Router } from '@angular/router';
import { NavbarComponent } from '../navbar/navbar.component';

@Component({
  selector: 'app-lockscreen',
  templateUrl: './lockscreen.component.html',
  styleUrls: ['./lockscreen.component.scss'],
  animations: [slideToTop()]
})
export class LockscreenComponent implements OnInit {
  showLockScreen: boolean = true;
  @Output() unlocked: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() logout: EventEmitter<any> = new EventEmitter<any>();

  constructor(private _router: Router) { }

  ngOnInit() {
  }

  unlockUser() {
    this.showLockScreen = false;
    let that = this;
    setTimeout(function() { that.unlocked.emit(false); },300);
  }

  logoutUser() {
    this.showLockScreen = false;
    let that = this;
    setTimeout(function() { that.logout.emit(); },300);
  }

  closeLockScreenPopup(callback) {
    this.showLockScreen = false;
    setTimeout(function () { callback }, 300);
  }
}
